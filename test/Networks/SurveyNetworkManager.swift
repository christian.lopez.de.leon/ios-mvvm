//
//  SurveyNetworkManager.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import Alamofire

class SurveyNetworkManager: NSObject {
    let headers: HTTPHeaders = [
        "Accept": "application/json"
    ]
    let baseUrl = "https://us-central1-bibliotecadecontenido.cloudfunctions.net"
    
    func getQuestions(completion: @escaping (SurveyResult?) -> Void) {
        AF.request("\(baseUrl)/helloWorld", method: .post, headers: headers).response { response in
            print("result:", response.result)
            guard let data = response.data else {
                completion(nil)
                return
            }
            do{
                let decoder = JSONDecoder()
                let cuentaRequest = try decoder.decode(SurveyResult.self, from: data)
                completion(cuentaRequest)
            } catch let error {
                print("error:", error)
                completion(nil)
            }
        }
    }
}
