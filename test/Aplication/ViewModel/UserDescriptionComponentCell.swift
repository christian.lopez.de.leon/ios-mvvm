//
//  UserDescriptionComponentCell.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import UIKit

class UserDescriptionComponentCell: UITableViewCell {
    static let cellIdentifier = "UserDescriptionComponentCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "UserDescriptionComponentCell", bundle: nil)
    }
}
