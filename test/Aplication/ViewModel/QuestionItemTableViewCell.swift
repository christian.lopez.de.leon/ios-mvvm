//
//  QuestionItemTableViewCell.swift
//  test
//
//  Created by Christian López on 11/8/21.
//

import UIKit
import Charts

class QuestionItemTableViewCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var pieChartView: PieChartView!
    
    static let cellIdentifier = "QuestionItemTableViewCell"
    fileprivate var colors: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "QuestionItemTableViewCell", bundle: nil)
    }
    
    func setupWithData(question: Question, colors: [String]) {
        self.questionLabel.text = question.question
        self.colors = colors
        customizeChart(charData: question.chartData)
    }
    
    func customizeChart(charData: [ChartItem]) {
        var dataEntries: [ChartDataEntry] = []
        charData.forEach { data in
            let dataEntry = PieChartDataEntry(value: Double(data.percetnage), label: data.answerd, data: data as AnyObject)
            dataEntries.append(dataEntry)
        }
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = colorsOfCharts(numbersOfColor: charData.count)
        
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        
        pieChartView.data = pieChartData
    }
    
    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
      var colors: [UIColor] = []
      for idx in 0..<numbersOfColor {
        colors.append(UIColor.colorFromHex(self.colors[idx]))
      }
      return colors
    }
    
    private func generateRandomColor() -> UIColor {
        let red = Double(arc4random_uniform(256))
        let green = Double(arc4random_uniform(256))
        let blue = Double(arc4random_uniform(256))
        return UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
    }
}
