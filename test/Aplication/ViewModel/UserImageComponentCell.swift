//
//  UserImageComponentCell.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import UIKit

class UserImageComponentCell: UITableViewCell {
    static let cellIdentifier = "UserImageComponentCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }

    private func setupUI() {}
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "UserImageComponentCell", bundle: nil)
    }
    
}
