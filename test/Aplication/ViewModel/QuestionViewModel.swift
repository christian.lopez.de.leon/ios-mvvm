//
//  QuestionViewModel.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import Foundation

class QuestionViewModel {
    var refreshData = { () -> () in }
    var cuestions: [Question] = [] {
        didSet {
            refreshData()
        }
    }
    var colors: [String] = []
    let surveyNetworkManager = SurveyNetworkManager()
    
    func getQuestions() {
        surveyNetworkManager.getQuestions() { response in
            guard let response = response else {
                return
            }
            self.cuestions = response.questions
            self.colors = response.colors
        }
    }
}
