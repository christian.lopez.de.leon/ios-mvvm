//
//  UserInformationComponentCell.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import UIKit

protocol UserInformationComponentCellDelegate {
    func usernameIs(text: String)
}

class UserInformationComponentCell: UITableViewCell {
    @IBOutlet weak var userNameTextField: UITextField!
    
    static let cellIdentifier = "UserInformationComponentCell"
    var delegate: UserInformationComponentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }

    private func setupUI() {
        self.userNameTextField.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "UserInformationComponentCell", bundle: nil)
    }
}

//MARK:- UITextFieldDelegate
extension UserInformationComponentCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: [])
            if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                return false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.delegate?.usernameIs(text: self.userNameTextField.text ?? "")
            }
        }
        catch {
            print("ERROR")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
