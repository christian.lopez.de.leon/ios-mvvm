//
//  UserPreviewComponentCell.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import UIKit

class UserPreviewComponentCell: UITableViewCell {
    @IBOutlet weak var userImagePreview: UIImageView!
    
    static let cellIdentifier = "UserPreviewComponentCell"
    var delegate: UserPreviewComponentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "UserPreviewComponentCell", bundle: nil)
    }
    
    func setupWithData(image: UIImage) {
        self.userImagePreview.image = image
    }
    
    @IBAction func takeOtherPhotoAction(_ sender: UIButton) {
        self.delegate?.fireSelfieOptionPopUp()
    }
    
}

protocol UserPreviewComponentCellDelegate {
    func fireSelfieOptionPopUp()
}
