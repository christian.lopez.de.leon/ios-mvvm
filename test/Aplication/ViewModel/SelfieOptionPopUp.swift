//
//  SelfieOptionPopUp.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import UIKit

class SelfieOptionPopUp: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var popUpView: UIView!
    
    let kCONTENT_XIB_NAME = "SelfieOptionPopUp"
    var delegate: ModalMainViewHelperDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func commonInit(container: UIView) {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(container)
    }

    @IBAction func closePopUpAction(_ sender: UIButton) {
        self.delegate?.dismisModalMainView()
        self.contentView.removeFromSuperview()
    }
    
    @IBAction func fromGalleryAction(_ sender: UIButton) {
        self.delegate?.onTapActionMainView(action: "fromGallery")
    }
    
    @IBAction func fromCameraAction(_ sender: UIButton) {
        self.delegate?.onTapActionMainView(action: "fromCamera")
    }
    
}
