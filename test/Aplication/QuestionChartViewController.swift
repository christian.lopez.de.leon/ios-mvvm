//
//  QuestionChartViewController.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import UIKit

class QuestionChartViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var questionViewModel = QuestionViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupTable()
        self.bind()
        self.questionViewModel.getQuestions()
    }
    
    private func setupUI() {
        self.title = "Preguntas"
    }
    
    private func setupTable() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(QuestionItemTableViewCell.nib(), forCellReuseIdentifier: QuestionItemTableViewCell.cellIdentifier)
    }
    
    private func bind() {
        questionViewModel.refreshData = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
}

extension QuestionChartViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questionViewModel.cuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: QuestionItemTableViewCell.cellIdentifier) as! QuestionItemTableViewCell
        cell.setupWithData(question: self.questionViewModel.cuestions[indexPath.row], colors: self.questionViewModel.colors)
        return cell
    }
    
    
}
