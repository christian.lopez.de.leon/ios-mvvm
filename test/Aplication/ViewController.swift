//
//  ViewController.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import UIKit
import FirebaseStorage
import SVProgressHUD
import FirebaseDatabase

class ViewController: UIViewController, UserInformationComponentCellDelegate, UserPreviewComponentCellDelegate {
    //MARK:- UI References
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var buttonUpLoadData: UIButton!
    
    //MARK:- Data
    fileprivate var homeComponents: [HomeComponents] = [] {
        didSet {
            updateMainDataSource()
        }
    }
    fileprivate var typeHome: TypeHome = .INVITED {
        didSet {
            getHomeComponents()
        }
    }
    var userName: String!
    private var imagePicker: UIImagePickerController?
    var modalMainView: UIView?
    var imageSaved: UIImage?
    var imageSavedData: Data?
    private var database = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupMainTableView()
        self.getHomeComponents()
        self.getBackGroundColor()
    }
    
    private func setupUI() {
        self.title = "Hola Mundo!"
        self.buttonUpLoadData.isEnabled = false
        self.buttonUpLoadData.backgroundColor = UIColor.gray
    }

    fileprivate func setupMainTableView() {
        self.mainTableView.dataSource = self
        self.mainTableView.delegate = self
        self.mainTableView.register(UserInformationComponentCell.nib(), forCellReuseIdentifier: UserInformationComponentCell.cellIdentifier)
        self.mainTableView.register(UserImageComponentCell.nib(), forCellReuseIdentifier: UserImageComponentCell.cellIdentifier)
        self.mainTableView.register(UserDescriptionComponentCell.nib(), forCellReuseIdentifier: UserDescriptionComponentCell.cellIdentifier)
        self.mainTableView.register(UserPreviewComponentCell.nib(), forCellReuseIdentifier: UserPreviewComponentCell.cellIdentifier)
    }
    
    private func getHomeComponents() {
        switch typeHome {
        case .INVITED:
            self.homeComponents = [
                .USER_INFORMATION,
                .USER_IMAGE,
                .USER_DESCRIPTION
            ]
        case .AUTENTICATED:
            self.homeComponents = [
                .USER_INFORMATION,
                .USER_PREVIEW,
                .USER_DESCRIPTION
            ]
        }
    }
    
    private func updateMainDataSource() {
        DispatchQueue.main.async {
            self.mainTableView.reloadData()
        }
    }
    
    func usernameIs(text: String) {
        self.userName = text
        self.validateInputs()
    }
    
    private func validateInputs() {
        if self.userName == nil || self.userName.isEmpty || self.imageSavedData == nil {
            self.buttonUpLoadData.isEnabled = false
            self.buttonUpLoadData.backgroundColor = UIColor.gray
        } else {
            self.buttonUpLoadData.isEnabled = true
            self.buttonUpLoadData.backgroundColor = .colorFromHex("#00568F")
        }
    }
    
    private func takeSelfie(from sourceType:
                                UIImagePickerController.SourceType) {
        self.dismisModalMainView()
        self.imagePicker = UIImagePickerController()
        self.imagePicker?.sourceType = sourceType
        self.imagePicker?.allowsEditing = true
        self.imagePicker?.delegate = self
        if sourceType == .photoLibrary {
            self.imagePicker?.mediaTypes = ["public.image"]
        }
        if sourceType == .camera {
            self.imagePicker?.cameraFlashMode = .off
            self.imagePicker?.cameraCaptureMode = .photo
        }
        guard let imagePicker = self.imagePicker else {
            return
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    func fireSelfieOptionPopUp() {
        self.setupModalMainView()
        guard let modalMainView = self.modalMainView else { return }
        let popUp = SelfieOptionPopUp()
        popUp.commonInit(container: modalMainView)
        popUp.delegate = self
        self.showModalMainView(view: popUp)
    }
    
    func onTapActionMainView(action: String) {
        switch action {
        case "fromCamera":
            self.takeSelfie(from: .camera)
        default:
            self.takeSelfie(from: .photoLibrary)
        }
    }
    
    private func uploadPhotoToFirebase() {
        guard let imageSavedData: Data = self.imageSavedData else {
                return
        }
        SVProgressHUD.show()
        let metaDataConfig = StorageMetadata()
        metaDataConfig.contentType = "image/jpg"
        let storage = Storage.storage()
        let imageName = "\(Int.random(in: 100...1000))-\(self.userName!)"
        let folderReference = storage.reference(withPath: "selfies/\(imageName).jpg")
        DispatchQueue.global(qos: .background).async {
            folderReference.putData(imageSavedData, metadata: metaDataConfig) { (metaData: StorageMetadata?, error: Error?) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    if let error = error {
                        print(error)
                        return
                    }
                    folderReference.downloadURL { (url: URL?, error: Error?) in
                        print(url?.absoluteString ?? "")
                    }
                }
            }
        }
    }
    
    private func goToQuestionsChart() {
        let storyboard = UIStoryboard(name: "QuestionChart", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuestionChartViewController") as! QuestionChartViewController
        present(vc, animated: true, completion: nil)
    }
    
    private func getBackGroundColor() {
        database.child("setup-ui").observe(.childChanged) { snapshot in
            guard let backgroundColor = snapshot.value as? String else {
                return
            }
            DispatchQueue.main.async {
                self.view.backgroundColor = UIColor.colorFromHex(backgroundColor)
                self.mainTableView.backgroundColor = UIColor.colorFromHex(backgroundColor)
            }
        }
    }

    @IBAction func tappedUploadDataButton(_ sender: UIButton) {
        self.uploadPhotoToFirebase()
    }

}

//MARK:- UITableViewDelegate and UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeComponents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let component = self.homeComponents[indexPath.row]
        switch component {
        case .USER_INFORMATION:
            let cell =  tableView.dequeueReusableCell(withIdentifier: UserInformationComponentCell.cellIdentifier, for: indexPath) as! UserInformationComponentCell
            cell.delegate = self
            return cell
        case .USER_IMAGE:
            let cell =  tableView.dequeueReusableCell(withIdentifier: UserImageComponentCell.cellIdentifier, for: indexPath) as! UserImageComponentCell
            return cell
        case .USER_DESCRIPTION:
            let cell =  tableView.dequeueReusableCell(withIdentifier: UserDescriptionComponentCell.cellIdentifier, for: indexPath) as! UserDescriptionComponentCell
            return cell
        case .USER_PREVIEW:
            let cell =  tableView.dequeueReusableCell(withIdentifier: UserPreviewComponentCell.cellIdentifier, for: indexPath) as! UserPreviewComponentCell
            cell.delegate = self
            cell.setupWithData(image: self.imageSaved!)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let component = self.homeComponents[indexPath.row]
        switch component {
        case .USER_INFORMATION:
            return
        case .USER_IMAGE:
            self.fireSelfieOptionPopUp()
        case .USER_DESCRIPTION:
            self.goToQuestionsChart()
        case .USER_PREVIEW:
            print("Tomar nueva foto")
        }
    }
}

//MARK:- UIImagePickerControllerDelegate
extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker?.dismiss(animated: true, completion: nil)
        if info.keys.contains(.originalImage) {
            self.imageSaved = info[.originalImage] as? UIImage
            guard let _ = imageSaved else {
                return
            }
            self.imageSavedData = imageSaved!.jpegData(compressionQuality: 0.1)
            self.typeHome = .AUTENTICATED
            self.validateInputs()
        }
    }
}

//MARK:- ModalMainViewHelperDelegate
extension ViewController: ModalMainViewHelperDelegate {
    func setupModalMainView() {
        self.modalMainView = UIView(frame: UIScreen.main.bounds)
        self.modalMainView?.backgroundColor = UIColor.colorFromHex("#000000").withAlphaComponent(0)
    }
    
    func dismisModalMainView() {
        self.modalMainView?.removeFromSuperview()
        self.modalMainView = nil
    }
    
    func showModalMainView(view: UIView) {
        self.modalMainView?.addSubview(view)
        self.modalMainView?.addToWindow()
    }
}

enum HomeComponents {
    case USER_INFORMATION
    case USER_IMAGE
    case USER_DESCRIPTION
    case USER_PREVIEW
}

enum TypeHome {
    case INVITED
    case AUTENTICATED
}

protocol ModalMainViewHelperDelegate {
    func setupModalMainView()
    func dismisModalMainView()
    func onTapActionMainView(action: String)
}
