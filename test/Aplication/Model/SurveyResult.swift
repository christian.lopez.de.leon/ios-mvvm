//
//  SurveyResult.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import Foundation

struct SurveyResult: Codable {
    let colors: [String]
    let questions: [Question]
    
    private enum CodingKeys: String, CodingKey {
        case colors, questions
    }
}
