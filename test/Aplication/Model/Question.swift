//
//  Question.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import Foundation

struct Question: Codable {
    let total: Int
    let question: String
    let chartData: [ChartItem]
    
    private enum CodingKeys: String, CodingKey {
        case total
        case question = "text"
        case chartData
    }
}
