//
//  ChartItem.swift
//  test
//
//  Created by Christian López on 11/7/21.
//

import Foundation

struct ChartItem: Codable {
    let answerd: String
    let percetnage: Int
    
    private enum CodingKeys: String, CodingKey {
        case answerd = "text"
        case percetnage
    }
}
